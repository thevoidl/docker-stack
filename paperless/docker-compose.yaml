services:
  broker:
    image: docker.io/library/redis:7
    restart: unless-stopped
    volumes:
      - redisdata:/data

  db:
    image: docker.io/library/postgres:15
    restart: unless-stopped
    volumes:
      - ./data/db:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: paperless
      POSTGRES_USER: paperless
      POSTGRES_PASSWORD: paperless

  # NOTE: paperless > 2.10 requires gotenberg 8
  webserver:
    image: ghcr.io/paperless-ngx/paperless-ngx:2.10.1
    restart: unless-stopped
    depends_on:
      - db
      - broker
      - gotenberg
      - tika
    volumes:
      - ./data/paperless/data:/usr/src/paperless/data
      - ./data/paperless/media:/usr/src/paperless/media
      - ./data/paperless/export:/usr/src/paperless/export
      - ./data/paperless/consume:/usr/src/paperless/consume
    environment:
      PAPERLESS_REDIS: redis://broker:6379
      PAPERLESS_DBHOST: db
      PAPERLESS_TIKA_ENABLED: 1
      PAPERLESS_TIKA_GOTENBERG_ENDPOINT: http://gotenberg:3000
      PAPERLESS_TIKA_ENDPOINT: http://tika:9998
      USERMAP_UID: 1001
      USERMAP_GID: 1001
      PAPERLESS_OCR_LANGUAGES: deu frk
      PAPERLESS_URL: https://$DOMAIN
      PAPERLESS_SECRET_KEY:
      PAPERLESS_OCR_LANGUAGE: deu
      PAPERLESS_EMAIL_HOST:
      PAPERLESS_EMAIL_PORT:
      PAPERLESS_EMAIL_FROM:
      PAPERLESS_EMAIL_HOST_USER: 
      PAPERLESS_EMAIL_HOST_PASSWORD: 
      PAPERLESS_EMAIL_USE_SSL:
      PAPERLESS_EMAIL_USE_TLS:
    labels:
      traefik.enable: true
      traefik.http.middlewares.redirect-https.redirectScheme.scheme: https
      traefik.http.middlewares.redirect-https.redirectScheme.permanent: true

      traefik.http.routers.paperless-https.rule: Host(`${DOMAIN}`)
      traefik.http.routers.paperless-https.entrypoints: websecure
      traefik.http.routers.paperless-https.tls: true

      traefik.http.routers.paperless-http.rule: Host(`${DOMAIN}`)
      traefik.http.routers.paperless-http.entrypoints: web
      traefik.http.routers.paperless-http.middlewares: redirect-https

      traefik.http.services.paperless.loadbalancer.server.port: 8000
      traefik.docker.network: proxy-tier
    networks:
      default:
      proxy-tier:

  # NOTE: paperless > 2.10 requires gotenberg 8
  gotenberg:
    image: docker.io/gotenberg/gotenberg:7.10
    restart: unless-stopped

    # The gotenberg chromium route is used to convert .eml files. We do not
    # want to allow external content like tracking pixels or even javascript.
    command:
      - "gotenberg"
      - "--chromium-disable-javascript=true"
      - "--chromium-allow-list=file:///tmp/.*"

  tika:
    image: ghcr.io/paperless-ngx/tika:latest
    restart: unless-stopped
    

volumes:
  redisdata:

networks:
  proxy-tier:
    external: true
