#!/bin/bash
## NOTE:: i never actively used mailman
##     :: but maybe one happens to use this file as template
if [ "$EUID" -ne 0 ]; then
  echo "Script must be run as root"
  exit
fi

MAILMAN_CONFIG_DIR=/opt/mailman # this is hardcoded in mailman's docker-compose file
MAILMAN_PROJECT_DIR=/opt/docker/mail/mailman
MAILCOW_PROJECT_DIR=/opt/docker/mail/mailcow

prompt_yn() {
  read -r -p "${1:-Are you sure?} [y/N] " response
  if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
      return 0;
  else
      return 1;
  fi
}

if prompt_yn "Add entries to docker-compose.override.yml in the mailcow directory?"; then
  cat <<EOF >> $MAILCOW_PROJECT_DIR/docker-compose.override.yml
services:
  postfix-mailcow:
    volumes:
      - /opt/mailman:/opt/mailman
    networks:
      - docker-mailman_mailman

networks:
  mailman_mailman:
    external: true
EOF
fi

# TODO check $MAILCOW_PROJECT_DIR/data/conf/postfix/main.cf for these variables
cat <<EOF >> $MAILCOW_PROJECT_DIR/data/conf/postfix/extra.cf
# mailman

recipient_delimiter = +
unknown_local_recipient_reject_code = 550
owner_request_special = no

local_recipient_maps =
  regexp:/opt/mailman/core/var/data/postfix_lmtp,
  proxy:unix:passwd.byname,
  \$alias_maps
virtual_mailbox_maps =
  proxy:mysql:/opt/postfix/conf/sql/mysql_virtual_mailbox_maps.cf,
  regexp:/opt/mailman/core/var/data/postfix_lmtp
transport_maps =
  pcre:/opt/postfix/conf/custom_transport.pcre,
  pcre:/opt/postfix/conf/local_transport,
  proxy:mysql:/opt/postfix/conf/sql/mysql_relay_ne.cf,
  proxy:mysql:/opt/postfix/conf/sql/mysql_transport_maps.cf,
  regexp:/opt/mailman/core/var/data/postfix_lmtp
relay_domains =
  proxy:mysql:/opt/postfix/conf/sql/mysql_virtual_relay_domain_maps.cf,
  regexp:/opt/mailman/core/var/data/postfix_domains
relay_recipient_maps =
  proxy:mysql:/opt/postfix/conf/sql/mysql_relay_recipient_maps.cf,
  regexp:/opt/mailman/core/var/data/postfix_lmtp
EOF


mkdir -p $MAILMAN_CONFIG_DIR/{core,web}
git clone https://github.com/maxking/docker-mailman $MAILMAN_PROJECT_DIR

echo "Admin user?"
read MAILMAN_ADMIN_USER
echo "Admin email?"
read MAILMAN_ADMIN_EMAIL
echo "Mailman domain?"
read MAILMAN_DOMAIN

cat <<EOF > $MAILMAN_PROJECT_DIR/.env
HYPERKITTY_API_KEY=`cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c30`
SECRET_KEY=`cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c30`
POSTGRES_PASSWORD=`cat /dev/urandom | tr -dc a-zA-Z0-9 | head -c30`
MAILMAN_ADMIN_USER=$MAILMAN_ADMIN_USER
MAILMAN_ADMIN_EMAIL=$MAILMAN_ADMIN_EMAIL
MAILMAN_DOMAIN=$MAILMAN_DOMAIN
EOF

cat <<EOF > $MAILMAN_PROJECT_DIR/docker-compose.override.yaml
services:
  mailman-core:
    environment:
    - DATABASE_URL=postgresql://mailman:DBPASS@database/mailmandb
    - HYPERKITTY_API_KEY
    - TZ=Europe/Berlin
    - MTA=postfix
    restart: always
    networks:
      - mailman

  mailman-web:
    environment:
    - DATABASE_URL=postgresql://mailman:DBPASS@database/mailmandb
    - HYPERKITTY_API_KEY=HYPERKITTY_KEY
    - TZ=Europe/Berlin
    - SECRET_KEY
    - SERVE_FROM_DOMAIN=MAILMAN_DOMAIN
    - MAILMAN_ADMIN_USER
    - MAILMAN_ADMIN_EMAIL
    - UWSGI_STATIC_MAP=/static=/opt/mailman-web-data/static
    restart: always

  database:
    environment:
    - POSTGRES_PASSWORD
    restart: always
EOF

echo Mailmal default FROM address? Must be a real smtp account.
read FROM_ADDRESS

cat <<EOF > $MAILMAN_CONFIG_DIR/core/mailman-extra.cfg
[mailman]
default_language: de
site_owner: $FROM_ADDRESS
EOF

cat <<EOF > $MAILMAN_CONFIG_DIR/web/settings_local.py
# locale
LANGUAGE_CODE = 'de-de'

# disable social authentication
MAILMAN_WEB_SOCIAL_AUTH = []

# change it
DEFAULT_FROM_EMAIL = '$FROM_ADDRESS'

DEBUG = False
EOF

docker compose -f $MAILMAN_PROJECT_DIR/docker-compose.yaml pull
