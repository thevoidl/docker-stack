## Listmonk

- docs: https://listmonk.app/docs
- docker installation: https://listmonk.app/docs/installation/
- i highly recommend setting up a dedicated bounce account: https://listmonk.app/docs/bounces/#pop3-bounce-mailbox
- advanced auth would be great, currently only `/admin` is basicauth-proteced by the application itself
