# Minimal configurable docker image for serving static HTTP resonse codes.

## Can be used in two modes:
  1. __port__: uses one distinct port for each configured response code, in the scheme (PORT + desiredResponseCode)
  2. __subdomain__: uses subdomains to determine the desired response code, in the scheme (desiredResponseCode.example.com)


## Configuration using environment variables

| name        | possible values                                                                | default         |
|-------------|--------------------------------------------------------------------------------|-----------------|
| STATUSCODES | most in the range 201-511                                                      | all             |
| METHODS     | 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS', 'TRACE', 'CONNECT' | all             |
| MODE        | 'port', 'subdomain'                                                            | port            |
| REALIP      | determine real IP if behind revers proxy                                       | X-Forwarded-For |
| PORT        |                                                                                | 8000            |

## Examples (using traefik)

```yaml
  captive-portal:
    image: staticresponse
    environment:
      STATUSCODES: 204
      METHODS: GET
      REALIP: X-Real-Ip
    expose:
      - 8204
    labels:
      traefik.enable: true
      traefik.http.routers.http204.rule: Host(`captiveportal.example.com`)
      traefik.http.routers.http204.entrypoints: web
      traefik.http.routers.http204.service: http204
      traefik.http.services.http204.loadbalancer.server.port: 8204

      traefik.http.routers.https204.tls: true
      traefik.http.routers.https204.tls.certResolver: myResolver
      traefik.http.routers.https204.rule: Host(`captiveportal.example.com`)
      traefik.http.routers.https204.entrypoints: websecure
      traefik.http.routers.https204.service: http204
      traefik.http.services.https204.loadbalancer.server.port: 8204
```

```yaml
  subdomains-responses:
    image: staticresponse
    environment:
      REALIP: X-Real-Ip
      PORT: 8080
      MODE: subdomain
    expose:
      - 8080
    labels:
      traefik.enable: true
      traefik.http.routers.http204.rule: HostRegexp(`.*.example.com`)
      traefik.http.routers.http204.entrypoints: web
      traefik.http.services.http204.loadbalancer.server.port: 8080
```

```yaml
  ports-static:
    image: staticresponse
    environment:
      STATUSCODES: 204,304,404,504
      METHODS: GET
      REALIP: X-Real-Ip
    expose:
      - 8204
      - 8304
      - 8404
      - 8504
    labels:
      traefik.enable: true
      traefik.http.routers.http204.rule: Host(`example.com`) && Path(`/204`)
      traefik.http.routers.http204.entrypoints: web
      traefik.http.routers.http204.service: http204
      traefik.http.services.http204.loadbalancer.server.port: 8204

      traefik.http.routers.http304.rule: Host(`example.com`) && Path(`/304`)
      traefik.http.routers.http304.entrypoints: web
      traefik.http.routers.http304.service: http304
      traefik.http.services.http304.loadbalancer.server.port: 8304

      traefik.http.routers.http404.rule: Host(`example.com`) && Path(`/404`)
      traefik.http.routers.http404.entrypoints: web
      traefik.http.routers.http404.service: http404
      traefik.http.services.http404.loadbalancer.server.port: 8404

      traefik.http.routers.http504.rule: Host(`example.com`) && Path(`/504`)
      traefik.http.routers.http504.entrypoints: web
      traefik.http.routers.http504.service: http504
      traefik.http.services.http504.loadbalancer.server.port: 8504
```
