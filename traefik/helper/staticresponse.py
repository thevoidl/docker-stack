from os import environ as env
from sys import stdout
import signal
from threading import Thread
from time import sleep
from http.server import BaseHTTPRequestHandler, HTTPServer, socketserver, socket

defaultPort = 8000
mode = 'port'
activeServers = []
forwardedHeader = env.get('REALIP', 'X-Forwarded-For')
allowedMethods = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS', 'TRACE', 'CONNECT']
allowedResponses = [201,202,203,204,205,206,207,208,226,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,451,500,501,502,503,504,505,506,507,508,509,510,511]

class Ipv6AbleServer(socketserver.TCPServer):
    address_family = socket.AF_INET6

class StaticResponse(BaseHTTPRequestHandler):
    server_version = 'StaticResponse/1.0'
    sys_version = ''

    def do_GET(self):
        self.respond()

    def do_HEAD(self):
        self.respond()

    def do_POST(self):
        self.respond()

    def do_PUT(self):
        self.respond()

    def do_DELETE(self):
        self.respond()

    def do_PATCH(self):
        self.respond()

    def do_OPTIONS(self):
        self.respond()

    def do_TRACE(self):
        self.respond()

    def do_CONNECT(self):
        self.respond()

    def log_message(self, format, *args):
        requestingIp = self.client_address[0]
        remoteIp = self.headers.get(forwardedHeader, requestingIp)
        remotePort = self.client_address[1]
        self.client_address = [remoteIp, remotePort]
        
        format += " %s"
        args += (self.headers["user-agent"],)
        BaseHTTPRequestHandler.log_message(self, format, *args)


    def respond(self):
        responseCode = 0
        requestMethod = self.command

        if requestMethod not in allowedMethods:
            write_log('Method not allowed:', requestMethod)
            responseCode = 405
        else:
            try:
                if mode == 'port':
                    responseCode = self.getPortResponseCode()
                elif mode == 'subdomain':
                    responseCode = self.getSubdomainResponseCode()

                if responseCode not in allowedResponses:
                    raise NotImplementedError('Response code not supported', responseCode)
            except Exception as e:
                write_log('Error:', e)
                return

        self.send_response(responseCode)
        self.send_header('cache-control', 'no-cache')
        self.end_headers()


    def getPort(self):
        return self.server.server_address[1]


    def getPortResponseCode(self):
        port = self.getPort()
        write_log('Request received on port', port)
        return port - defaultPort


    def getSubdomainResponseCode(self):
        requestedHost = self.headers.get('Host')
        if requestedHost.count('.') < 2:
            write_log('No subdomain found in', requestedHost)
            return

        subdomain = requestedHost.split('.')[0]
        try:
            responseCode = int(subdomain)
        except:
            write_log('Invalid response code:', subdomain)
            raise ValueError('Invalid response code:', subdomain)

        return responseCode


def write_log(*message):
    print(*message)
    stdout.flush()

def run(port):
    print('Listening on port', port)
    server_address = ('', port)
    httpd = Ipv6AbleServer(server_address, StaticResponse)

    with httpd:
        activeServers.append(httpd)
        httpd.serve_forever()


def startServers(port):
    print('Starting application for response codes:', allowedResponses)
    print('Allowed methods:', allowedMethods)
    print('Mode:', mode)
    print('----------------------------------------')

    try:
        if mode == 'subdomain':
            thread = Thread(target=run, args=(port,))
            thread.start()

        elif mode == 'port':
            for response in allowedResponses:
                port = response + defaultPort
                thread = Thread(target=run, args=(port,))
                thread.start()
                sleep(0.1)

    except Exception as e:
        print('Error starting server on port', port)
        print(e)


def exitHandler(sig, frame):
    print('----------------------------------------')
    print('Received signal', sig)
    print('Stopping servers...')
    for server in activeServers:
        server.shutdown()
        server.server_close()
    print('Servers stopped')

signal.signal(signal.SIGINT, exitHandler)
signal.signal(signal.SIGTERM, exitHandler)
signal.signal(signal.SIGTSTP, exitHandler)

if __name__ == '__main__':

    try:
        port = int(env.get('PORT', defaultPort))

        if env.get('STATUSCODES'):
            userResponseCodes  = [int(code) for code in env.get('STATUSCODES').split(',')]
            for response in userResponseCodes:
                if int(response) not in allowedResponses:
                    raise NotImplementedError('Response code not supported', response)
            allowedResponses = userResponseCodes

        if env.get('METHODS'):
            userMethods = env.get('METHODS').split(',')
            for method in userMethods:
                if method not in allowedMethods:
                    raise NotImplementedError('Method not supported', method)
            allowedMethods = userMethods

        if env.get('MODE'):
            if env.get('MODE') != 'subdomain' and env.get('MODE') != 'port':
                raise AssertionError('Invalid mode', env.get('MODE'))
            mode = env.get('MODE')

        startServers(port)

    except Exception as e:
        print('Error parsing environment variables')
        print(e)
